(function($) {

	var openaidmap;
	Drupal.behaviors.openlayersgeoclusterzoom = {
		attach : function(context, settings) {
			var data = $(context).data('openlayers');
			if (data && data.map.behaviors.openlayers_geocluster_behavior_zoom) {
				openaidmap = data.openlayers;
				var options = data.map.behaviors.openlayers_geocluster_behavior_zoom;
				var layers = [];

				// For backwards compatiability, if layers is not
				// defined, then include all vector layers
				if (typeof options.layers == 'undefined'
						|| options.layers.length == 0) {
					layers = openaidmap
							.getLayersByClass('OpenLayers.Layer.Vector');
				} else {
					for ( var i in options.layers) {
						var selectedLayer = openaidmap.getLayersBy('drupalID',
								options.layers[i]);
						if (typeof selectedLayer[0] != 'undefined') {
							layers.push(selectedLayer[0]);
							var selectCtrl = new OpenLayers.Control.SelectFeature(
									selectedLayer[0],
									{
										  clickout: false, 
										  toggle: false,
									       multiple: false, 
									       toggleKey: "ctrlKey", // ctrl key removes from selection
									       multipleKey: "shiftKey", // shift key adds to selection
										hover : true,
										callbacks : {
											// Callback for feature click
											'click' : Drupal.openlayersgeoclusterzoom.click,
											// Callback for feature hover
											'over' : Drupal.openlayersgeoclusterzoom.over,
											// Callback for out feature
											'out' : Drupal.openlayersgeoclusterzoom.out,

										}
									});

							openaidmap.addControl(selectCtrl);
							selectCtrl.activate();
						}
					}
				}
			}
		}
	};

	Drupal.openlayersgeoclusterzoom = {
		// Click state
		'click' : function(feature) {
			var bounds;
			var lonlat;
			Drupal.openlayersgeoclusterzoom.out(feature);
			currentZoomLevel = openaidmap.getZoom();
			bounds = feature.geometry.getBounds();
			lonlat = bounds.getCenterLonLat();
			openaidmap.setCenter(lonlat, currentZoomLevel + 1);
		},

		//Hover state
		'over' : function(feature) {
			var elem_id = feature.geometry.id;
			document.getElementById(elem_id).style.cursor = "pointer";

			var tooltip = $('<span class="popup"></span>');
			var text = feature.attributes.description;

			var point = new OpenLayers.LonLat(feature.geometry.x,
					feature.geometry.y);
			var offset = feature.layer.getViewPortPxFromLonLat(point);
			tooltip.html(text).show();
			tooltip.css({
				zIndex : '1000',
				position : 'absolute',
				left : offset.x,
				top : offset.y
			});
			$(feature.layer.map.div).css({
				position : 'relative'
			}).append(tooltip);
		},
		//out state
		'out' : function(feature) {
			$(feature.layer.map.div).children().each(function(index) {
				if (this.tagName === 'SPAN') {
					$(this).parent().children('span').remove();
				}

			});
		}

	};

})(jQuery);
