<?php
/**
 * @file
 * Behaviour that adds functionality to the geocluster module
 */

class openlayers_geocluster_behavior_zoom extends openlayers_behavior {
  /**
   * Override of options_init().
   */
  public function options_init() {
    return array();
  }
  
  /**
   * Override of options_form().
   */
  public function options_form($defaults = array()) {
    $vector_layers = array();
    foreach ($this->map['layers'] as $id => $name) {
      $layer = openlayers_layer_load($id);
      if (isset($layer->data['vector']) && $layer->data['vector'] == TRUE) {
        $vector_layers[$id] = $name;
      }
    }

    $options['layers'] = array(
      '#title' => t('Layers'),
      '#type' => 'checkboxes',
      '#options' => $vector_layers,
      '#description' => t('Select layers for Geocluster Openlayers.'),
      '#default_value' => isset($defaults['layers']) ? $defaults['layers'] : NULL,
    );
  
    return $options;
  }
  
  /**
   * Render.
   */
  public function render(&$map) {
  	drupal_add_css(drupal_get_path('module', 'geocluster_openlayers') . '/plugins/behaviors/openlayers_geocluster_behavior_zoom.css');
    drupal_add_js(drupal_get_path('module', 'geocluster_openlayers') . '/plugins/behaviors/openlayers_geocluster_behavior_zoom.js');
    
    return $this->options;
  }
}
